# Tetris

Tetris written in Vue.js, using vuex modules.

## Control

-   left arrow - move block left
-   right arrow - move block right
-   up arrow, f or r - rotate block
-   space - drop block
-   p - pause/resume

## Demo

[Live demo here](https://sandorfarkas.gitlab.io/tetris/)
