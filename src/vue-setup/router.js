import Vue from 'vue';
import VueRouter from 'vue-router';
import TetrisPage from 'src/components/tetris-page';

Vue.use(VueRouter);

const routes = [{ path: '/', component: TetrisPage }];

export const router = new VueRouter({
	routes,
	mode: 'history',
	base: process.env.PUBLIC_PATH,
});
