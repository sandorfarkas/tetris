import Vuex from 'vuex';
import Vue from 'vue';
import { Store } from 'src/store';
import { TetrisStore } from 'src/store';
import { GfxStore } from 'src/store';
import { ControlStore } from 'src/store';

Vue.use(Vuex);

// Export for production use
export const store = VuexStore();

// Export for testing, maybe development
export function VuexStore({
	strict = process.env.NODE_ENV !== 'production',
} = {}) {
	return new Vuex.Store({
		...Store({ localStorage: window.localStorage }),
		modules: {
			tetris: { ...TetrisStore({ localStorage: window.localStorage }) },
			gfx: { ...GfxStore() },
			control: { ...ControlStore() },
		},
		strict,
	});
}
