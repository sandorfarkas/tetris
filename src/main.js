import './style/main.scss';

import Vue from 'vue';
import * as vueSetup from './vue-setup';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
	faArrowLeft,
	faArrowUp,
	faArrowRight,
	faArrowDown,
	faAngleDoubleDown,
	faUndo,
	faRedo,
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faArrowLeft);
library.add(faArrowUp);
library.add(faArrowRight);
library.add(faArrowDown);
library.add(faAngleDoubleDown);
library.add(faUndo);
library.add(faRedo);

Vue.component('FontAwesomeIcon', FontAwesomeIcon);

new Vue({ el: '#app', render: (h) => h('router-view'), ...vueSetup });
