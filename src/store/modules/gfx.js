export function GfxStore() {
	// move to state and getters
	const width = window.innerWidth > 0 ? window.innerWidth : screen.width;
	const PIECE_SIDE = width <= 1100 ? 20 : 25;
	const PIECE_GAP = width <= 1100 ? 2 : 3;
	const BLOCK_FULL_WIDTH = PIECE_SIDE + PIECE_GAP;

	const state = {
		boardCanvas: null,
		boardContext: null,
		nextBlockCanvas: null,
		nextBlockContext: null,
	};

	const getters = {
		boardFullWidth(_, getters) {
			return getters.boardWidth * BLOCK_FULL_WIDTH - PIECE_GAP;
		},
		boardFullHeight(_, getters) {
			return getters.boardHeight * BLOCK_FULL_WIDTH - PIECE_GAP;
		},
		getColor(type) {
			switch (type) {
				case 0:
					return 'black';
				case 1:
					return 'red';
				case 2:
					return 'green';
				case 3:
					return 'blue';
				case 4:
					return 'hotpink';
				case 5:
					return 'orange';
				case 6:
					return 'purple';
				default:
					return;
			}
		},
		blockFullWidth() {
			return BLOCK_FULL_WIDTH;
		},
	};

	const mutations = {
		setBoardListener(state, { type, listener }) {
			state.boardCanvas.addEventListener(type, listener, false);
		},
		setCanvasesAndContexts(state, { boardCanvas, nextBlockCanvas }) {
			if (boardCanvas) {
				state.boardCanvas = boardCanvas;
				state.boardContext = boardCanvas.getContext('2d');
			}
			if (nextBlockCanvas) {
				state.nextBlockCanvas = nextBlockCanvas;
				state.nextBlockContext = nextBlockCanvas.getContext('2d');
			}
		},
		clearBoardCanvas(state) {
			state.boardContext.clearRect(
				0,
				0,
				state.boardContext.canvas.width,
				state.boardContext.canvas.height
			);
			state.boardContext.beginPath();
		},
		clearNextBlockCanvas(state) {
			state.nextBlockContext.clearRect(
				0,
				0,
				state.nextBlockContext.canvas.width,
				state.nextBlockContext.canvas.height
			);
			state.nextBlockContext.beginPath();
		},
		drawPiece(_, { x, y, color, context, shadow }) {
			context.shadowBlur = shadow ? 6 : 0;
			context.shadowOffsetX = shadow ? 2 : 0;
			context.shadowOffsetY = shadow ? 2 : 0;
			context.shadowColor = shadow;
			context.fillStyle = color;
			context.fillRect(x, y, PIECE_SIDE, PIECE_SIDE);
		},
		drawBorderForBlock(state, { upperLeftX, upperLeftY, blockTemplate }) {
			state.boardContext.beginPath();
			state.boardContext.arc(
				upperLeftX + BLOCK_FULL_WIDTH / 2,
				upperLeftY + BLOCK_FULL_WIDTH / 2,
				BLOCK_FULL_WIDTH / 2,
				0,
				2 * Math.PI,
				false
			);
			state.boardContext.lineWidth = 1;
			state.boardContext.strokeStyle = 'red';
			state.boardContext.stroke();

			state.boardContext.beginPath();
			state.boardContext.fillStyle = 'black';
			state.boardContext.rect(
				upperLeftX,
				upperLeftY,
				BLOCK_FULL_WIDTH * blockTemplate[0].length,
				BLOCK_FULL_WIDTH * blockTemplate.length
			);
			state.boardContext.stroke();
		},
	};

	const actions = {
		initGfx({ commit }, { boardCanvas, nextBlockCanvas }) {
			commit('setCanvasesAndContexts', { boardCanvas, nextBlockCanvas });
		},
		setBoardListener({ commit }, listener) {
			commit('setBoardListener', listener);
		},
		redraw({ dispatch, getters }) {
			dispatch('clearBoardCanvas');
			dispatch('drawCurrentBoard');
			dispatch('drawCurrentBlock');
			if (getters.isDevMode === true) {
				dispatch('drawBorderForBlock');
			}
		},
		clearBoardCanvas({ commit }) {
			commit('clearBoardCanvas');
		},
		drawCurrentBoard({ state, commit, rootGetters }) {
			const currentBoard = rootGetters.currentBoard;

			for (let x = 0; x < rootGetters.boardWidth; x++) {
				for (let y = 0; y < rootGetters.boardHeight; y++) {
					if (currentBoard[y][x] !== -1) {
						commit('drawPiece', {
							x: x * BLOCK_FULL_WIDTH,
							y: y * BLOCK_FULL_WIDTH,
							color: getters.getColor(currentBoard[y][x]),
							context: state.boardContext,
							shadow: 'black',
						});
					}
				}
			}
		},
		drawBlock(
			{ commit },
			{ context, blockTemplate, upperLeftX, upperLeftY, blockToDraw }
		) {
			for (let row = 0; row < blockTemplate.length; row++) {
				for (
					let column = 0;
					column < blockTemplate[row].length;
					column++
				) {
					if (blockTemplate[row][column] === 1) {
						const pieceUpperLeftX =
							upperLeftX + column * BLOCK_FULL_WIDTH;
						const pieceUpperLeftY =
							upperLeftY + row * BLOCK_FULL_WIDTH;

						commit('drawPiece', {
							x: pieceUpperLeftX,
							y: pieceUpperLeftY,
							color: getters.getColor(blockToDraw.type),
							context,
							shadow: 'brown',
						});
					}
				}
			}
		},
		drawCurrentBlock({ dispatch, rootGetters }) {
			const currentBlock = rootGetters.currentBlock;
			const upperLeftX = currentBlock.x * BLOCK_FULL_WIDTH;
			const upperLeftY = currentBlock.y * BLOCK_FULL_WIDTH;
			const blockTemplate = rootGetters.currentBlockTemplate;

			dispatch('drawBlock', {
				context: state.boardContext,
				blockTemplate,
				upperLeftX,
				upperLeftY,
				blockToDraw: currentBlock,
			});
		},
		drawNextBlock({ commit, dispatch, rootGetters }) {
			commit('clearNextBlockCanvas');
			const nextBlock = rootGetters.nextBlock;
			const blockTemplate = rootGetters.nextBlockTemplate;
			const blockWidth = blockTemplate[0].length;
			const horizontalShift =
				BLOCK_FULL_WIDTH +
				(blockWidth === 3 ? BLOCK_FULL_WIDTH / 2 : 0);

			dispatch('drawBlock', {
				context: state.nextBlockContext,
				blockTemplate,
				upperLeftX: horizontalShift,
				upperLeftY: horizontalShift,
				blockToDraw: nextBlock,
			});
		},
		drawBorderForBlock({ commit, rootGetters }) {
			const currentBlock = rootGetters.currentBlock;
			const upperLeftX = currentBlock.x * BLOCK_FULL_WIDTH;
			const upperLeftY = currentBlock.y * BLOCK_FULL_WIDTH;
			const blockTemplate = rootGetters.currentBlockTemplate;

			commit('drawBorderForBlock', {
				upperLeftX,
				upperLeftY,
				blockTemplate,
			});
		},
		async animateRemovingFullRows({ commit, rootGetters }, rows) {
			const currentBoard = rootGetters.currentBoard;

			function timeout(ms) {
				return new Promise((resolve) => setTimeout(resolve, ms));
			}

			for (let i = 0; i < 5; i++) {
				for (let x = 0; x < rootGetters.boardWidth; x++) {
					for (let y = 0; y < rootGetters.boardHeight; y++) {
						if (currentBoard[y][x] !== -1) {
							commit('drawPiece', {
								x: x * BLOCK_FULL_WIDTH,
								y: y * BLOCK_FULL_WIDTH,
								color:
									i % 2 == 0 && rows.includes(y)
										? getters.getColor(i)
										: getters.getColor(currentBoard[y][x]),
								context: state.boardContext,
							});
						}
					}
				}

				await timeout(70);
			}
		},
	};

	return { state, getters, mutations, actions };
}
