import { TetrisStore } from './teris';
import { blockTemplates } from 'src/domain/block-templates';

let tetrisStore;

describe('Tetris store module', () => {
	beforeEach(() => {
		const localStorage = {
			getItem: jest.fn(() => null),
			setItem: jest.fn(),
		};
		tetrisStore = TetrisStore({ localStorage });
	});

	test('should return score', () => {
		expect(tetrisStore.getters.score({ score: 123 })).toBe(123);
	});

	test('addToScore should increase score with given value', () => {
		const state = {
			score: 0,
		};
		tetrisStore.mutations.addToScore(state, 234);

		expect(state.score).toBe(234);
	});

	test('loadHighScore should set highscore from local store', () => {
		const localStorage = {
			getItem: jest.fn(() => 345),
			setItem: jest.fn(),
		};
		tetrisStore = TetrisStore({ localStorage });

		const state = {
			highScore: null,
		};

		tetrisStore.mutations.loadHighScore(state);

		expect(state.highScore).toBe(345);
	});

	test('loadHighScore should set 0 as highscore if no prev value is stored', () => {
		const state = {
			highScore: null,
		};

		tetrisStore.mutations.loadHighScore(state);

		expect(state.highScore).toBe(0);
	});

	test('highScore should return highscore when highscore > score', () => {
		const state = { score: 99, highScore: 100 };

		expect(tetrisStore.getters.highScore(state)).toBe(100);
	});

	test('highScore should return score when highscore < score', () => {
		const state = { score: 98, highScore: 99 };

		expect(tetrisStore.getters.highScore(state)).toBe(99);
	});

	test('high score should return score if score is higher than high score', () => {});

	test('saveHighScore should save high score to local storage', () => {
		const setItem = jest.fn();
		const localStorage = { getItem: jest.fn(), setItem };
		tetrisStore = TetrisStore({ localStorage });

		tetrisStore.mutations.saveHighScore({}, 456);

		expect(setItem).toHaveBeenCalledWith('highScore', 456);
	});

	test('Reset score should set score 0', () => {
		const state = { score: 567 };

		tetrisStore.mutations.resetScore(state);

		expect(state.score).toBe(0);
	});

	test('initTetris shoud reset score', () => {
		const commit = jest.fn();
		const dispatch = jest.fn();
		const getters = jest.fn();

		tetrisStore.actions.initTetris({ commit, dispatch, getters });

		expect(commit).toHaveBeenCalledWith('resetScore');
	});

	test('Reset level should set score 0', () => {
		const state = { level: 353 };

		tetrisStore.mutations.resetLevel(state);

		expect(state.level).toBe(0);
	});

	test('initTetris shoud reset level', () => {
		const commit = jest.fn();
		const dispatch = jest.fn();
		const getters = jest.fn();

		tetrisStore.actions.initTetris({ commit, dispatch, getters });

		expect(commit).toHaveBeenCalledWith('resetLevel');
	});

	test('tear down should set game over', () => {
		const commit = jest.fn();
		tetrisStore.actions.tearDownGame({
			commit,
			getters: { score: 0, highScore: 0 },
		});

		expect(commit).toHaveBeenCalledWith('setGameOver', true);
	});

	test('should return level', () => {
		const state = { level: 33 };

		expect(tetrisStore.getters.level(state)).toBe(33);
	});

	test('calculate first piece position', () => {
		// OOOO
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][0] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][2] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][3] }
			)
		).toBe(1);

		// O
		// OOO
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][0] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][1] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][2] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][3] }
			)
		).toBe(0);

		//   O
		// OOO
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][0] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][1] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][2] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][3] }
			)
		).toBe(0);

		// OO
		// OO
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][0] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][1] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][2] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][3] }
			)
		).toBe(1);

		//  OO
		// OO
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][0] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][1] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][2] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][3] }
			)
		).toBe(0);

		//  O
		// OOO
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][0] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][1] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][2] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][3] }
			)
		).toBe(0);

		// OO
		//  OO
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][0] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][1] }
			)
		).toBe(1);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][2] }
			)
		).toBe(0);
		expect(
			tetrisStore.getters.getPositionOfLeftmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][3] }
			)
		).toBe(0);
	});

	test('calculate last piece position', () => {
		// OOOO
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][0] }
			)
		).toBe(3);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][2] }
			)
		).toBe(3);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[0][3] }
			)
		).toBe(1);

		// O
		// OOO
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][0] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][2] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[1][3] }
			)
		).toBe(1);

		//   O
		// OOO
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][0] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][2] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[2][3] }
			)
		).toBe(1);

		// OO
		// OO
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][0] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][2] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[3][3] }
			)
		).toBe(2);

		//  OO
		// OO
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][0] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][2] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[4][3] }
			)
		).toBe(1);

		//  O
		// OOO
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][0] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][2] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[5][3] }
			)
		).toBe(1);

		// OO
		//  OO
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][0] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][1] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][2] }
			)
		).toBe(2);
		expect(
			tetrisStore.getters.getPositionOfRightmostPiece(
				{},
				{ currentBlockTemplate: blockTemplates[6][3] }
			)
		).toBe(1);
	});
});
