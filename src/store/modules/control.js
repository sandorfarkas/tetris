export function ControlStore() {
	const state = {
		touchXDown: null,
		touchYDown: null,
	};

	const getters = {
		touchXDown({ touchXDown }) {
			return touchXDown;
		},
		touchYDown({ touchYDown }) {
			return touchYDown;
		},
	};

	const mutations = {
		setTouchXDown(state, touchXDown) {
			state.touchXDown = touchXDown;
		},
		setTouchYDown(state, touchYDown) {
			state.touchYDown = touchYDown;
		},
	};

	const actions = {
		moveLeft({ dispatch }) {
			dispatch('updateCurrentBlock', { x: -1 });
			dispatch('redraw');
		},
		moveRight({ dispatch }) {
			dispatch('updateCurrentBlock', { x: 1 });
			dispatch('redraw');
		},
		moveDown({ dispatch }) {
			dispatch('movePieceDown');
			dispatch('redraw');
		},
		rotateClockwise({ dispatch }) {
			dispatch('rotateCurrentBlockClockwise');
			dispatch('redraw');
		},
		rotateCounterClockwise({ dispatch }) {
			dispatch('rotateCurrentBlockCounterClockwise');
			dispatch('redraw');
		},
		initKeyboardListener({ dispatch }) {
			const listener = (event) => {
				if (event.isComposing || event.keyCode === 229) {
					return;
				}
				// left pressed
				if (event.keyCode === 37) {
					event.preventDefault();
					dispatch('moveLeft');
					return;
				}
				// right pressed
				if (event.keyCode === 39) {
					event.preventDefault();
					dispatch('moveRight');
					return;
				}
				// down pressed
				if (event.keyCode === 40) {
					event.preventDefault();
					dispatch('moveDown');
					return;
				}
				// up, 'f' or 'r' pressed
				if (
					event.keyCode === 38 ||
					event.keyCode === 70 ||
					event.keyCode === 82
				) {
					event.preventDefault();

					dispatch('rotateClockwise');
					return;
				}
				// 'e' pressed
				if (event.keyCode === 69) {
					dispatch('rotateCounterClockwise');
					return;
				}
				// space pressed
				if (event.keyCode === 32) {
					event.preventDefault();
					dispatch('dropCurrentBlock');
					return;
				}
				// p pressed
				if (event.keyCode === 80) {
					dispatch('togglePause');
				}
			};
			dispatch('setBoardListener', { type: 'keydown', listener });
		},
		initTouchListener({ commit, dispatch, getters }) {
			const getTouches = (event) => {
				return (
					event.touches || // browser API
					event.originalEvent.touches
				); // jQuery
			};

			const handleTouchStart = (event) => {
				event.preventDefault();
				const firstTouch = getTouches(event)[0];
				commit('setTouchXDown', firstTouch.clientX);
				commit('setTouchYDown', firstTouch.clientY);
			};

			const handleTouchMove = (event) => {
				event.preventDefault();
				if (!getters.touchXDown || !getters.touchYDown) {
					return;
				}

				const xUp = event.touches[0].clientX;
				const yUp = event.touches[0].clientY;

				const xDiff = getters.touchXDown - xUp;
				const yDiff = getters.touchYDown - yUp;

				if (Math.abs(xDiff) > Math.abs(yDiff)) {
					/*most significant*/
					if (xDiff > 0) {
						/* left swipe */
						dispatch('moveLeft');
					} else {
						/* right swipe */
						dispatch('moveRight');
					}
				} else {
					if (yDiff > 0) {
						/* up swipe */
						dispatch('rotateClockwise');
					} else {
						/* down swipe */
						for (let i = 0; i < Math.round(Math.abs(yDiff)); i++) {
							dispatch('moveDown');
						}
					}
				}
				/* reset values */
				commit('setTouchXDown', null);
				commit('setTouchYDown', null);
			};
			dispatch('setBoardListener', {
				type: 'touchstart',
				listener: handleTouchStart,
			});
			dispatch('setBoardListener', {
				type: 'touchmove',
				listener: handleTouchMove,
			});
		},
	};

	return { state, getters, mutations, actions };
}
