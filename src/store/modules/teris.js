import { blockTemplates } from 'src/domain/block-templates';

export function TetrisStore({ localStorage }) {
	const state = {
		boardWidth: 12,
		boardHeight: 22,
		currentBoard: null,
		currentBlock: null,
		nextBlock: null,
		score: 0,
		highScore: null,
		gameOver: false,
		level: 0,
	};

	const getters = {
		boardWidth(state) {
			return state.boardWidth;
		},
		boardHeight(state) {
			return state.boardHeight;
		},
		currentBoard(state) {
			return state.currentBoard;
		},
		currentBlock(state) {
			return state.currentBlock;
		},
		nextBlock(state) {
			return state.nextBlock;
		},
		currentBlockX(state) {
			return state.currentBlock !== null
				? state.currentBlock.x
				: undefined;
		},
		currentBlockY(state) {
			return state.currentBlock !== null
				? state.currentBlock.y
				: undefined;
		},
		currentBlockTemplate(state) {
			return blockTemplates[state.currentBlock.type][
				state.currentBlock.rotation
			];
		},
		nextBlockTemplate(state) {
			return blockTemplates[state.nextBlock.type][
				state.nextBlock.rotation
			];
		},
		score(state) {
			return state.score;
		},
		highScore(state) {
			return state.highScore;
		},
		currentHighScore(state) {
			return state.score > state.highScore
				? state.score
				: state.highScore;
		},
		isGameOver(state) {
			return state.gameOver;
		},
		level(state) {
			return state.level;
		},
		getPositionOfLeftmostPiece(_, getters) {
			const blockTemplate = getters.currentBlockTemplate;
			for (let x = 0; x < blockTemplate[0].length; x++) {
				for (let y = 0; y < blockTemplate.length; y++) {
					if (blockTemplate[y][x] === 1) {
						return x;
					}
				}
			}
		},
		getPositionOfRightmostPiece(_, getters) {
			const blockTemplate = getters.currentBlockTemplate;
			for (let x = blockTemplate[0].length - 1; x >= 0; x--) {
				for (let y = blockTemplate.length - 1; y >= 0; y--) {
					if (blockTemplate[y][x] === 1) {
						return x;
					}
				}
			}
		},
		validateBlockPosition: (state) => (deltaX, deltaY) => {
			const blockTemplate = getters.currentBlockTemplate(state);
			let outOfBoard = false;
			let stateInvalid = false;

			for (let column = 0; column < blockTemplate[0].length; column++) {
				for (let row = 0; row < blockTemplate.length; row++) {
					const pieceAbsoluteX =
						state.currentBlock.x + column + deltaX;
					const pieceAbsoluteY = state.currentBlock.y + row + deltaY;

					// out of board
					if (
						blockTemplate[row][column] === 1 &&
						(pieceAbsoluteX > state.boardWidth - 1 ||
							pieceAbsoluteX < 0)
					) {
						outOfBoard = true;
						continue;
					}

					if (
						blockTemplate[row][column] === 1 &&
						pieceAbsoluteY > state.boardHeight - 1
					) {
						outOfBoard = true;
						continue;
					}

					// on another piece
					if (
						blockTemplate[row][column] === 1 &&
						pieceAbsoluteY >= 0 &&
						state.currentBoard[pieceAbsoluteY][pieceAbsoluteX] !==
							-1
					) {
						stateInvalid = true;
					}
				}
			}

			return { outOfBoard, stateInvalid };
		},
	};

	const mutations = {
		loadHighScore(state) {
			const storedHighScore = localStorage.getItem('highScore');
			state.highScore =
				storedHighScore === null ? 0 : parseInt(storedHighScore);
		},
		setCurrentBoard(state, currentBoard) {
			state.currentBoard = currentBoard;
		},
		setCurrentBlock(state, currentBlock) {
			state.currentBlock = currentBlock;
		},
		setNextBlock(state, nextBlock) {
			state.nextBlock = nextBlock;
		},
		updateCurrentBlock(state, { x, y }) {
			if (x !== undefined) {
				state.currentBlock.x += x;
			}

			if (y !== undefined) {
				state.currentBlock.y += y;
			}
		},
		updateCurrentBoard(state, { x, y, type }) {
			state.currentBoard[y][x] = type;
		},
		rotateClockwise(state) {
			state.currentBlock.rotation++;
			if (state.currentBlock.rotation === 4) {
				state.currentBlock.rotation = 0;
			}
		},
		rotateCounterClockwise(state) {
			state.currentBlock.rotation--;
			if (state.currentBlock.rotation === -1) {
				state.currentBlock.rotation = 3;
			}
		},
		addToScore(state, value) {
			state.score += value;
		},
		resetScore(state) {
			state.score = 0;
		},
		resetLevel(state) {
			state.level = 0;
		},
		saveHighScore(_, score) {
			localStorage.setItem('highScore', score);
		},
		setGameOver(state, gameOver) {
			state.gameOver = gameOver;
		},
		levelUp(state) {
			state.level++;
		},
	};

	const actions = {
		initTetris({ commit, dispatch, getters }) {
			commit('setGameOver', false);
			commit('resetScore');
			commit('resetLevel');
			commit('loadHighScore');

			dispatch('generateNewBoard');
			dispatch('generateNewBlock');
			dispatch('drawCurrentBlock');
			if (getters.isDevMode === true) {
				dispatch('drawBorderForBlock');
			}
		},
		generateNewBoard({ commit, getters }) {
			commit(
				'setCurrentBoard',
				[...Array(getters.boardHeight)].map(() =>
					[...Array(getters.boardWidth)].map(() => -1)
				)
			);
		},
		generateNewBlock({ getters, commit, dispatch }) {
			const type = Math.floor(Math.random() * 7);
			const rotation = 0;
			const shiftX =
				blockTemplates[type][rotation][0].length === 4 ? 2 : 1;
			const shiftY = type === 0 ? 1 : 0;

			const createNewBlock = () => ({
				x: getters.boardWidth / 2 - shiftX,
				y: 0 - shiftY,
				type,
				rotation,
			});

			commit(
				'setCurrentBlock',
				getters.currentBlock === null
					? createNewBlock()
					: getters.nextBlock
			);
			commit('setNextBlock', createNewBlock());

			const result = getters.validateBlockPosition(0, 0);
			if (result.stateInvalid) {
				dispatch('tearDownGame');
			}
		},
		saveHighScore({ commit, getters }) {
			if (getters.score > getters.highScore) {
				commit('saveHighScore', getters.score);
			}
		},
		tearDownGame({ commit }) {
			commit('setGameOver', true);
		},
		async removeFullRows({ getters, commit, dispatch }) {
			const newBoard = [...Array(getters.boardHeight)].map(() =>
				[...Array(getters.boardWidth)].map(() => -1)
			);
			let newBoardRow = getters.boardHeight - 1;
			const removedRows = [];

			const handleScore = (removedRowsLength) => {
				commit('addToScore', getters.boardWidth * removedRowsLength);
				if (getters.score > getters.highScore) {
					commit('saveHighScore', getters.score);
				}
				if (getters.score > 600 + getters.level * 600) {
					commit('levelUp');
					dispatch('startLoop');
				}
			};

			for (let row = getters.boardHeight - 1; row >= 0; row--) {
				let rowIsFull = true;
				for (let column = 0; column < getters.boardWidth; column++) {
					if (getters.currentBoard[row][column] === -1) {
						rowIsFull = false;
					}
				}
				if (rowIsFull) {
					removedRows.push(row);
					handleScore(removedRows.length);
				} else {
					for (
						let column = 0;
						column < getters.boardWidth;
						column++
					) {
						newBoard[newBoardRow][column] =
							getters.currentBoard[row][column];
					}
					newBoardRow--;
				}
			}
			if (removedRows.length > 0) {
				dispatch('stopLoop');
				await dispatch('animateRemovingFullRows', removedRows);
				commit('setCurrentBoard', newBoard);
				dispatch('startLoop');
			}
		},
		addBlockToCurrentBoard({ commit, dispatch, getters }) {
			const blockTemplate = getters.currentBlockTemplate;
			for (let column = 0; column < blockTemplate[0].length; column++) {
				for (let row = 0; row < blockTemplate.length; row++) {
					if (blockTemplate[row][column] === 1) {
						commit('updateCurrentBoard', {
							x: getters.currentBlockX + column,
							y: getters.currentBlockY + row,
							type: getters.currentBlock.type,
						});
					}
				}
			}
			dispatch('removeFullRows');
		},
		updateCurrentBlock({ commit, dispatch, getters }, { x, y }) {
			if (getters.isGameOver || !getters.isRunning) {
				return;
			}
			if (x !== undefined) {
				const result = getters.validateBlockPosition(x, 0);
				if (!result.stateInvalid && !result.outOfBoard) {
					commit('updateCurrentBlock', { x });
				}
			}

			if (y !== undefined) {
				const result = getters.validateBlockPosition(0, 1);

				if (result.outOfBoard || result.stateInvalid) {
					dispatch('addBlockToCurrentBoard');
					dispatch('generateNewBlock');
					return;
				}
				commit('updateCurrentBlock', { y });
			}
		},
		rotateCurrentBlockClockwise({ getters, commit }) {
			if (getters.isGameOver || !getters.isRunning) {
				return;
			}
			commit('rotateClockwise');
			while (
				getters.currentBlockX + getters.getPositionOfLeftmostPiece <
				0
			) {
				commit('updateCurrentBlock', { x: 1 });
			}
			while (
				getters.currentBlockX + getters.getPositionOfRightmostPiece >
				getters.boardWidth - 1
			) {
				commit('updateCurrentBlock', { x: -1 });
			}

			const result = getters.validateBlockPosition(0, 0);
			if (result.stateInvalid) {
				commit('rotateCounterClockwise');
			}
		},
		rotateCurrentBlockCounterClockwise({ getters, commit }) {
			if (getters.isGameOver || !getters.isRunning) {
				return;
			}
			commit('rotateCounterClockwise');
			while (
				getters.currentBlockX + getters.getPositionOfLeftmostPiece <
				0
			) {
				commit('updateCurrentBlock', { x: 1 });
			}
			while (
				getters.currentBlockX + getters.getPositionOfRightmostPiece >
				getters.boardWidth - 1
			) {
				commit('updateCurrentBlock', { x: -1 });
			}

			const result = getters.validateBlockPosition(0, 0);
			if (result.stateInvalid) {
				commit('rotateClockwise');
			}
		},
		movePieceDown({ dispatch, getters }) {
			if (getters.isGameOver) {
				return;
			}
			dispatch('updateCurrentBlock', { y: 1 });
		},
		dropCurrentBlock({ dispatch, getters }) {
			if (getters.isGameOver || !getters.isRunning) {
				return;
			}
			const block = getters.currentBlock;
			while (block === getters.currentBlock) {
				dispatch('updateCurrentBlock', { y: 1 });
				dispatch('redraw');
			}
		},
	};

	return { state, getters, mutations, actions };
}
