export function Store({ localStorage }) {
	const state = {
		loopTimer: null,
		devMode: false,
	};

	const getters = {
		isRunning(state) {
			return state.loopTimer !== null;
		},
		isDevMode(state) {
			return state.devMode;
		},
	};

	const mutations = {
		setLoopTimer(state, loopTimer) {
			state.loopTimer = loopTimer;
		},
		stopLoop(state) {
			clearInterval(state.loopTimer);
			state.loopTimer = null;
		},
		setTetrisState(state, newTetrisState) {
			state.tetris = newTetrisState;
		},
	};

	const actions = {
		async initGame({ commit, dispatch }) {
			dispatch('initKeyboardListener');
			dispatch('initTouchListener');

			if (localStorage.getItem('state')) {
				commit(
					'setTetrisState',
					JSON.parse(localStorage.getItem('state'))
				);
				dispatch('redraw');
			} else {
				dispatch('initTetris');
			}
			dispatch('startLoop');
		},
		startLoop({ commit, dispatch, getters }) {
			commit('stopLoop');
			const calculateSpeed =
				getters.level < 10 ? 1000 - getters.level * 100 : 100;

			dispatch('movePieceDown');
			dispatch('redraw');

			const loopTimer = setInterval(function () {
				if (getters.isGameOver) {
					commit('stopLoop');
				} else {
					dispatch('movePieceDown');
					dispatch('redraw');
				}
			}, calculateSpeed);
			commit('setLoopTimer', loopTimer);
		},
		stopLoop({ commit }) {
			commit('stopLoop');
		},
		togglePause({ commit, dispatch, getters }) {
			if (getters.isRunning) {
				commit('stopLoop');
			} else {
				dispatch('startLoop');
			}
		},
		restart({ dispatch }) {
			dispatch('initTetris');
			dispatch('redraw');
			dispatch('startLoop');
		},
		saveState({ state }) {
			if (state.tetris.gameOver) {
				localStorage.removeItem('state');
			} else {
				localStorage.setItem('state', JSON.stringify(state.tetris));
			}
		},
	};

	return { state, getters, mutations, actions };
}
