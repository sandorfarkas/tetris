export { Store } from './store';
export { TetrisStore } from './modules/teris';
export { GfxStore } from './modules/gfx';
export { ControlStore } from './modules/control';
